const express = require("express");

const itemRoutes = require("./routes/item.route");
const baseRoute = require("./routes/base.route");

const app = express();
const port = 3000;

app.use(express.json());

app.use("/", baseRoute);
app.use("/items", itemRoutes);

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
