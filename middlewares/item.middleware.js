const validateRequestBody = (req, res, next) => {
  const body = req.body;
  if (Object.keys(body).length === 0) {
    return res.status(400).json({
      message: "Request body cannot be empty",
      status: "error",
    });
  }

  next();
};

module.exports = { validateRequestBody };
